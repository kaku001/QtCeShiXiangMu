﻿#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include "transform.cpp"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    WaveIn(new SingleChaanle),
    WaveInSignal(new SingleChaanle),
    WaveImpuls(new SingleChaanle),
    WaveFreq(new SingleChaanle),
    WaveOut(new SingleChaanle),
    WaveSignalOut(new SingleChaanle),
    LabFilterType(new QLabel(QString::fromLocal8Bit("滤波器类型:"))),
    cBoxFilterType(new QComboBox),
    hLayoutFilter(new QHBoxLayout),
    LabWinType(new QLabel(QString::fromLocal8Bit("窗口类型:"))),
    cBoxWinType(new QComboBox),
    hLayoutWin(new QHBoxLayout),
    LabScala(new QLabel(QString::fromLocal8Bit("阶数:"))),
    EditScala(new QLineEdit),
    hLayoutScala(new QHBoxLayout),
    LabFreq(new QLabel(QString::fromLocal8Bit("采样频率:"))),
    EditFreq(new QLineEdit),
    hLayoutFreq(new QHBoxLayout),
    LabUpperFreq(new QLabel(QString::fromLocal8Bit("频率上限:"))),
    EditUpperFreq(new QLineEdit),
    hLayoutUpperFreq(new QHBoxLayout),
    LabLowerFreq(new QLabel(QString::fromLocal8Bit("频率下限:"))),
    EditLowerFreq(new QLineEdit),
    hLayoutLowerFreq(new QHBoxLayout),
    BtnCalcWave(new QPushButton(QString::fromLocal8Bit("计算"))),
    vLayoutWave(new QVBoxLayout),
    vLayoutSignal(new QVBoxLayout),
    vLayoutOp(new QVBoxLayout),
    hLayoutMain(new QHBoxLayout)
{
    ui->setupUi(this);
    vLayoutWave->addWidget(WaveIn);
    vLayoutWave->addWidget(WaveImpuls);
    vLayoutWave->addWidget(WaveOut);
    vLayoutSignal->addWidget(WaveInSignal);
    vLayoutSignal->addWidget(WaveFreq);
    vLayoutSignal->addWidget(WaveSignalOut);
    vLayoutOp->addWidget(ui->BtnReadFile);
    hLayoutFilter->addWidget(LabFilterType);
    hLayoutFilter->addWidget(cBoxFilterType);
    vLayoutOp->addLayout(hLayoutFilter);
    hLayoutWin->addWidget(LabWinType);
    hLayoutWin->addWidget(cBoxWinType);
    vLayoutOp->addLayout(hLayoutWin);
    hLayoutScala->addWidget(LabScala);
    hLayoutScala->addWidget(EditScala);
    vLayoutOp->addLayout(hLayoutScala);
    hLayoutFreq->addWidget(LabFreq);
    hLayoutFreq->addWidget(EditFreq);
    vLayoutOp->addLayout(hLayoutFreq);
    hLayoutUpperFreq->addWidget(LabUpperFreq);
    hLayoutUpperFreq->addWidget(EditUpperFreq);
    vLayoutOp->addLayout(hLayoutUpperFreq);
    hLayoutLowerFreq->addWidget(LabLowerFreq);
    hLayoutLowerFreq->addWidget(EditLowerFreq);
    vLayoutOp->addLayout(hLayoutLowerFreq);
    vLayoutOp->addWidget(BtnCalcWave);
    vLayoutOp->addStretch();
    hLayoutMain->addLayout(vLayoutWave);
    hLayoutMain->addLayout(vLayoutSignal);
    hLayoutMain->addLayout(vLayoutOp);
    LabFilterType->setAlignment(Qt::AlignRight);
    LabWinType->setAlignment(Qt::AlignRight);
    LabScala->setAlignment(Qt::AlignRight);
    LabFreq->setAlignment(Qt::AlignRight);
    LabUpperFreq->setAlignment(Qt::AlignRight);
    LabLowerFreq->setAlignment(Qt::AlignRight);
    hLayoutMain->setStretch(0, 48);
    hLayoutMain->setStretch(1, 48);
    hLayoutMain->setStretch(2, 4);
    setLayout(hLayoutMain);
    showMaximized();
    // 参数设置
    cBoxFilterType->addItem(QString::fromLocal8Bit("低通"));
    cBoxFilterType->addItem(QString::fromLocal8Bit("高通"));
    cBoxFilterType->addItem(QString::fromLocal8Bit("带通"));
    cBoxFilterType->addItem(QString::fromLocal8Bit("带阻"));
    cBoxWinType->addItem(QString::fromLocal8Bit("矩形"));
    cBoxWinType->addItem(QString::fromLocal8Bit("图基"));
    cBoxWinType->addItem(QString::fromLocal8Bit("三角"));
    cBoxWinType->addItem(QString::fromLocal8Bit("汉宁"));
    cBoxWinType->addItem(QString::fromLocal8Bit("海明"));
    cBoxWinType->addItem(QString::fromLocal8Bit("布拉克曼"));
    cBoxWinType->addItem(QString::fromLocal8Bit("凯塞"));
    FilterType = Filter::LOWPASS;
    WinType = Filter::RECTANGLE;
    Scala = 64;
    EditScala->setText(QString::number(Scala));
    Freq = 1000;
    EditFreq->setText(QString::number(Freq));
    LowerFreq = 100;
    EditLowerFreq->setText(QString::number(LowerFreq));
    UpperFreq = 300;
    EditUpperFreq->setText(QString::number(UpperFreq));
    // 属性、功能设置
    connect(BtnCalcWave, &QPushButton::clicked, this, &Widget::BtnCalc_clicked);
    connect(cBoxFilterType, SIGNAL(currentIndexChanged(int)), this, SLOT(cBoxFilterType_curIndexChanged(int)));
    connect(cBoxWinType, SIGNAL(currentIndexChanged(int)), this, SLOT(cBoxWinType_curIndexChanged(int)));
    connect(EditScala, &QLineEdit::editingFinished, this, &Widget::EditScala_editingFinished);
    connect(EditFreq, &QLineEdit::editingFinished, this, &Widget::EditFreq_editingFinished);
    connect(EditLowerFreq, &QLineEdit::editingFinished, this, &Widget::EditLowerFreq_editingFinished);
    connect(EditUpperFreq, &QLineEdit::editingFinished, this, &Widget::EditUpperFreq_editingFinished);
}

Widget::~Widget()
{
    LabFilterType->deleteLater();
    cBoxFilterType->deleteLater();
    hLayoutFilter->deleteLater();
    LabWinType->deleteLater();
    cBoxWinType->deleteLater();
    hLayoutWin->deleteLater();
    LabScala->deleteLater();
    EditScala->deleteLater();
    hLayoutScala->deleteLater();
    LabFreq->deleteLater();
    EditFreq->deleteLater();
    hLayoutFreq->deleteLater();
    LabUpperFreq->deleteLater();
    EditUpperFreq->deleteLater();
    hLayoutUpperFreq->deleteLater();
    LabLowerFreq->deleteLater();
    EditLowerFreq->deleteLater();
    hLayoutLowerFreq->deleteLater();
    BtnCalcWave->deleteLater();
    WaveIn->deleteLater();
    WaveInSignal->deleteLater();
    WaveImpuls->deleteLater();
    WaveFreq->deleteLater();
    WaveOut->deleteLater();
    WaveSignalOut->deleteLater();
    vLayoutWave->deleteLater();
    vLayoutSignal->deleteLater();
    vLayoutOp->deleteLater();
    hLayoutMain->deleteLater();
    delete ui;
}

void Widget::on_BtnReadFile_clicked()
{
    QString file = QFileDialog::getOpenFileName(this);
    if(file.isEmpty())
        return;
    SampData.ReadDataFile(file);
    qDebug() << "Type:" << SampData.Header.bfType <<
                "bfSize:" << SampData.Header.bfSize <<
                "diVer:" << SampData.Header.diVer <<
                "diADBit:" << SampData.Header.diADBit <<
                "diSize:" << SampData.Header.diSize <<
                "diSampleFreq:" << SampData.Header.diSampleFreq <<
                "diChannelNum:" << SampData.Header.diChannelNum <<
                "diCh:" << SampData.Header.diCh <<
                "diTestPointNum:" << SampData.Header.diTestPointNum <<
                "diSensitivity:" << SampData.Header.diSensitivity <<
                "diMultiple:" << SampData.Header.diMultiple <<
                "diUnit:" << SampData.Header.diUnit <<
                "diRemark:" << SampData.Header.diRemark <<
                "diDataFlag:" << SampData.Header.diDataFlag;
    int Len = 1;
    while(Len < SampData.Header.diSize)
        Len *= 2;
    Len /= 2;
    valarray<complex<long double>> pp(static_cast<size_t>(Len));
    valarray<complex<long double>> f(static_cast<size_t>(Len));
    QVector<double> x(SampData.Header.diSize), y(SampData.Header.diSize);
    double MaxValue = 0;
    for (auto i = 0; i < SampData.Header.diSize; i++) {
        x[i] = i;
//        if(i < 1000 || i > 4000)
//            SampData.Data[i] = -100;
//        else
//            SampData.Data[i] = 500;
        y[i] = static_cast<double>(SampData.Data[i]);
        if(i < Len)
            pp[static_cast<size_t>(i)] = complex<long double>(static_cast<long double>(SampData.Data[i]), static_cast<long double>(0.0));
        if(fabs(y[i]) > MaxValue)
            MaxValue = fabs(y[i]);
    }
    WaveIn->SetxAxisRange(0, SampData.Header.diSize);
    WaveIn->SetyAxisRange(-MaxValue, MaxValue);
    WaveIn->SetData(x, y);
    // 输入信号频谱
    FourierTransform(pp, f, 0, 1);
    double MaxFreqS = 0, MinFreqS = 0;
    QVector<double> xFreq(Len / 2 + 1), yFreqS(Len / 2 + 1);
    for (auto i = 0; i < Len / 2 + 1; i++) {
        xFreq[i] = i / ((Len / 2 + 1) / (Freq / 2));
        yFreqS[i] = static_cast<double>(pp[static_cast<size_t>(i)].real() / (Len / 2 + 1) * 2);
        if(yFreqS[i] > MaxFreqS)
            MaxFreqS = yFreqS[i];
        if(yFreqS[i] < MinFreqS)
            MinFreqS = yFreqS[i];
    }
    WaveInSignal->SetxAxisRange(0, Freq / 2);
    WaveInSignal->SetyAxisRange(MinFreqS, MaxFreqS);
    WaveInSignal->SetData(xFreq, yFreqS);
//    qDebug() << "setp:" << (1.0 / (1.0 / Freq));
}

void Widget::BtnCalc_clicked()
{
    if(SampData.Data == nullptr)
        return;
    Filter filter(FilterType, WinType, Scala, Freq, LowerFreq, UpperFreq);
    QVector<double> xImpuls(Scala), yImpuls(Scala);
    double MaxImpuls = 0, MinImpuls = 0;
    for (auto i = 0; i < Scala; i++) {
        xImpuls[i] = i;
        yImpuls[i] = filter.ImpactData[i];
        if(yImpuls[i] > MaxImpuls)
            MaxImpuls = yImpuls[i];
        if(yImpuls[i] < MinImpuls)
            MinImpuls = yImpuls[i];
    }
    WaveImpuls->SetxAxisRange(0, Scala);
    WaveImpuls->SetyAxisRange(MinImpuls, MaxImpuls);
    WaveImpuls->SetData(xImpuls, yImpuls);
    // 输出波形
    filter.Convolution(SampData.Data, SampData.Header.diSize);
    if(filter.OutData == nullptr)
        return;
    int Len = 1;
    while(Len < SampData.Header.diSize)
        Len *= 2;
    Len /= 2;
    valarray<complex<long double>> pp(static_cast<size_t>(Len));
    valarray<complex<long double>> f(static_cast<size_t>(Len));
    QVector<double> xOut(SampData.Header.diSize), yOut(SampData.Header.diSize);
    double MaxOut = 0, MinOut = 0;
    for (auto i = 0; i < SampData.Header.diSize; i++) {
        xOut[i] = i;
        yOut[i] = filter.OutData[i];
        if(i < Len)
            pp[static_cast<size_t>(i)] = complex<long double>(static_cast<long double>(yOut[i]), static_cast<long double>(0.0));
        if(yOut[i] > MaxOut)
            MaxOut = yOut[i];
        if(yOut[i] < MinOut)
            MinOut = yOut[i];
    }
    WaveOut->SetxAxisRange(0, SampData.Header.diSize);
    WaveOut->SetyAxisRange(MinOut, MaxOut);
    WaveOut->SetData(xOut, yOut);
    // 滤波器的频率响应
    FourierTransform(pp, f, 0, 1);
    double MaxFreqS = 0, MinFreqS = 0;
    QVector<double> xFreq(Len / 2 + 1), yFreqS(Len / 2 + 1);
    for (auto i = 0; i < Len / 2 + 1; i++) {
        xFreq[i] = i / ((Len / 2 + 1) / (Freq / 2));
        yFreqS[i] = static_cast<double>(pp[static_cast<size_t>(i)].real() / (Len / 2 + 1) * 2);
        if(yFreqS[i] > MaxFreqS)
            MaxFreqS = yFreqS[i];
        if(yFreqS[i] < MinFreqS)
            MinFreqS = yFreqS[i];
    }
    WaveSignalOut->SetxAxisRange(0, Freq / 2);
    WaveSignalOut->SetyAxisRange(MinFreqS, MaxFreqS);
    WaveSignalOut->SetData(xFreq, yFreqS);
    // 频率响应
    valarray<complex<long double>> Impulspp(static_cast<size_t>(Len));
    valarray<complex<long double>> Impulsf(static_cast<size_t>(Len));
    for (auto i = 0; i < Len; i++) {
        if(i < Scala)
            Impulspp[static_cast<size_t>(i)] = complex<long double>(static_cast<long double>(yImpuls[i]), static_cast<long double>(0.0));
        else
            Impulspp[static_cast<size_t>(i)] = complex<long double>(static_cast<long double>(0.0), static_cast<long double>(0.0));
    }
    FourierTransform(Impulspp, Impulsf, 0, 1);
    double MaxF = 0, MinF = 0;
    QVector<double> xF(Len / 2 + 1), yF(Len / 2 + 1);
    for (auto i = 0; i < Len / 2 + 1; i++) {
        xF[i] = i / ((Len / 2 + 1) / (Freq / 2));
        yF[i] = 20.0 * log(fabs(static_cast<double>(Impulspp[static_cast<size_t>(i)].real())));
        if(yF[i] > MaxF)
            MaxF = yF[i];
        if(yF[i] < MinF)
            MinF = yF[i];
    }
    WaveFreq->SetxAxisRange(0, Freq / 2);
    WaveFreq->SetyAxisRange(MinF, MaxF);
    WaveFreq->SetData(xF, yF);
}

void Widget::cBoxFilterType_curIndexChanged(int index)
{
    FilterType = index;
}

void Widget::cBoxWinType_curIndexChanged(int index)
{
    WinType = index;
}

void Widget::EditScala_editingFinished()
{
    Scala = EditScala->text().toInt();
}

void Widget::EditFreq_editingFinished()
{
    Freq = EditFreq->text().toDouble();
}

void Widget::EditLowerFreq_editingFinished()
{
    LowerFreq = EditLowerFreq->text().toDouble();
}

void Widget::EditUpperFreq_editingFinished()
{
    UpperFreq = EditUpperFreq->text().toDouble();
}

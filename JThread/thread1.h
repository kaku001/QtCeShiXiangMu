﻿#ifndef THREAD1_H
#define THREAD1_H

#include <QObject>
#include <QThread>
#include <QMutex>

class Thread1 : public QObject
{
    Q_OBJECT
public:
    explicit Thread1(QObject *parent = nullptr);

    void Stop();

signals:

public slots:
    void doWork();

private:
    bool IsStop;
    QMutex StopMutex;
};

#endif // THREAD1_H

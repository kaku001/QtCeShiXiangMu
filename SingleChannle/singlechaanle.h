﻿#ifndef SINGLECHAANLE_H
#define SINGLECHAANLE_H

#include <QWidget>
#include <QMouseEvent>
#include <QEvent>
#include <QRubberBand>
#include "qcustomplot.h"


class ChWaveInfo{
public:
    int ChNumber;       // 通道号
    float Frequency;    // 采集频率
    float SampTime;     // 采集时长，单位：s
    double Value;        // 值
    ChWaveInfo(int ChNum = 0, float Freq = 0, float Time = 0, double Val = 0) :
        ChNumber(ChNum),
        Frequency(Freq),
        SampTime(Time),
        Value(Val) {}
};

namespace Ui {
class SingleChaanle;
}

class SingleChaanle : public QWidget
{
    Q_OBJECT

public:
    explicit SingleChaanle(QWidget *parent = nullptr);
    ~SingleChaanle();
private slots:
    void on_Plot_customContextMenuRequested(const QPoint &pos);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    // 选中曲线
    void selectionChanged();

protected:
    void Init();
    QString GetWaveInfo(const ChWaveInfo& Info);
private:
    Ui::SingleChaanle *ui;
protected:
    QRubberBand *RubBand;
    QPoint StartPoint;
    ChWaveInfo WaveInfo;
    double Xup;
    double Xdown;
    double Yup;
    double Ydown;
};

#endif // SINGLECHAANLE_H

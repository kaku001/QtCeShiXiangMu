﻿#include "jmainframe.h"
#include <QDebug>
#include <QApplication>

/***************自定义按钮***************/
JButton::JButton(int Orientation, QWidget *parent)
    : QWidget(parent)
    , btnOrientation(Orientation)
    , btnSize(42, 52)
    , labelIcon(new QLabel("icon"))
    , labelText(new QLabel("text"))
    , vLayMain(new QVBoxLayout)
{
    Init(Orientation);
}

JButton::JButton(const QIcon &icon, const QString &text, int Orientation, QWidget *parent)
    : QWidget(parent)
    , btnOrientation(Orientation)
    , btnSize(42, 52)
    , labelIcon(new QLabel)
    , labelText(new QLabel)
    , vLayMain(new QVBoxLayout)
    , btnIcon(icon.pixmap(btnSize))
    , btnText(text)
{
    Init(btnOrientation);
}

JButton::JButton(const QPixmap &icon, const QString &text, int Orientation, QWidget *parent)
    : QWidget(parent)
    , btnOrientation(Orientation)
    , btnSize(42, 52)
    , labelIcon(new QLabel)
    , labelText(new QLabel)
    , vLayMain(new QVBoxLayout)
    , btnIcon(icon)
    , btnText(text)
{
    Init(btnOrientation);
}

JButton::JButton(const JButton &btn)
    : btnOrientation(btn.btnOrientation)
    , btnSize(btn.btnSize)
    , btnRatio(btn.btnRatio)
    , labelIcon(new QLabel)
    , labelText(new QLabel)
    , vLayMain(new QVBoxLayout)
    , btnIcon(btn.btnIcon)
    , btnText(btn.btnText)
{
    Init(btnOrientation);
}

JButton::~JButton()
{
    labelIcon->deleteLater();
    labelText->deleteLater();
    vLayMain->deleteLater();
}

void JButton::Init(int Orientation)
{
    btnRatio.iconRatio = 4;
    btnRatio.textRatio = 1;
    setAttribute(Qt::WA_StyledBackground, true);
    labelText->setAlignment(Qt::AlignCenter);
    vLayMain->addWidget(labelIcon);
    vLayMain->addWidget(labelText);
    setLayout(vLayMain);
    vLayMain->setContentsMargins(2, 1, 2, 1);
    vLayMain->setSpacing(0);
    setOrientation(Orientation);
    setBtnRatio(btnRatio);
    setIcon(btnIcon);
    setText(btnText);
}

void JButton::setBtnSize(const QSize &size)
{
    this->resize(size);
    setBtnRatio(btnRatio);
    setIcon(btnIcon);
}

void JButton::setBtnRatio(const JButton::JRatio &ratio)
{
    vLayMain->setStretch(0, ratio.iconRatio);
    vLayMain->setStretch(0, ratio.textRatio);
    float temp = static_cast<float>((ratio.iconRatio * 1.0) / (ratio.iconRatio + ratio.textRatio));
    int iconHeight = static_cast<int>(btnSize.height() * temp);
    labelIcon->resize(size().width(), iconHeight);
    labelText->resize(size().width(), size().height() - iconHeight);
}

void JButton::mouseReleaseEvent(QMouseEvent *event)
{
    if(Qt::LeftButton == event->button()) {
        emit clicked();
    }
}

void JButton::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

void JButton::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

void JButton::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

QSize JButton::size() const
{
    return btnSize;
}

void JButton::setSize(const QSize &size)
{
    btnSize = size;
    setBtnSize(btnSize);
}

void JButton::setSize(int width, int height)
{
    btnSize.setWidth(width);
    btnSize.setHeight(height);
    setBtnSize(btnSize);
}

JButton::JRatio JButton::ratio() const
{
    return btnRatio;
}

void JButton::setRatio(const JRatio &ratio)
{
    btnRatio = ratio;
    setBtnRatio(btnRatio);
}

void JButton::setRatio(int iconRatio, int textRatio)
{
    btnRatio.iconRatio = iconRatio;
    btnRatio.textRatio=  textRatio;
    setBtnRatio(btnRatio);
}

void JButton::setOrientation(int Orientation)
{
    switch (Orientation) {
    case UP:
        vLayMain->setDirection(QBoxLayout::TopToBottom);
        break;
    case DOWN:
        vLayMain->setDirection(QBoxLayout::BottomToTop);
        break;
    case LEFT:
        vLayMain->setDirection(QBoxLayout::LeftToRight);
        break;
    case RIGHT:
        vLayMain->setDirection(QBoxLayout::RightToLeft);
        break;
    default:
        break;
    }
}

void JButton::setIcon(const QIcon &icon)
{
    labelIcon->clear();
    labelIcon->setPixmap(icon.pixmap(labelIcon->width(), labelIcon->height()));
}

void JButton::setIcon(const QPixmap &icon)
{
    labelIcon->clear();
    labelIcon->setPixmap(icon.scaled(labelIcon->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
}

void JButton::setText(const QString &text)
{
    labelText->setText(text);
}





/**************自定义标题栏***************/

JTitleBar::JTitleBar(QWidget *parent)
    : QWidget(parent)
    , btnLogo(new QPushButton)
    , hLayTitle(new QHBoxLayout)
    , hLayButtons(new QHBoxLayout)
    , btnMin(new QPushButton)
    , btnMax(new QPushButton)
    , btnClose(new QPushButton)
    , hLayWindow(new QHBoxLayout)
    , vLayWindow(new QVBoxLayout)
    , titleHeight(50)
    , logoWidth(260)
    , leftButtonState(false)
{
    Init();
}

JTitleBar::JTitleBar(const JTitleBar &title)
    : btnLogo(new QPushButton)
    , hLayTitle(new QHBoxLayout)
    , hLayButtons(new QHBoxLayout)
    , btnMin(new QPushButton)
    , btnMax(new QPushButton)
    , btnClose(new QPushButton)
    , hLayWindow(new QHBoxLayout)
    , vLayWindow(new QVBoxLayout)
    , titleHeight(title.titleHeight)
    , logoWidth(title.logoWidth)
    , leftButtonState(title.leftButtonState)
{
    Init();
}

JTitleBar::~JTitleBar()
{
    btnLogo->deleteLater();
    for (auto i = btnList.size() - 1; i >= 0; i--) {
        btnList.at(i)->deleteLater();
    }
    hLayButtons->deleteLater();
    btnMin->deleteLater();
    btnMax->deleteLater();
    btnClose->deleteLater();
    hLayWindow->deleteLater();
    vLayWindow->deleteLater();
    hLayTitle->deleteLater();
}

void JTitleBar::Init()
{
    // 使设置setStyleSheet生效
    setAttribute(Qt::WA_StyledBackground, true);
    // set objectName
    btnLogo->setObjectName("btnLogo");
    btnMin->setObjectName("btnMin");
    btnMax->setObjectName("btnMax");
    btnClose->setObjectName("btnClose");
    // layout
    hLayTitle->addWidget(btnLogo);
    hLayTitle->addLayout(hLayButtons);
    hLayTitle->addStretch();
    hLayWindow->addWidget(btnMin);
    hLayWindow->addWidget(btnMax);
    hLayWindow->addWidget(btnClose);
    vLayWindow->addLayout(hLayWindow);
    vLayWindow->addStretch();
    hLayTitle->addLayout(vLayWindow);
    setLayout(hLayTitle);
    hLayTitle->setContentsMargins(0, 0, 0, 0);
    hLayTitle->setSpacing(0);
    hLayButtons->setContentsMargins(0, 15, 15, 10);
    hLayButtons->setSpacing(10);
    hLayWindow->setContentsMargins(5, 5, 5, 5);
    hLayWindow->setSpacing(1);
    btnMin->setFixedSize(32, 20);
    btnMax->setFixedSize(32, 20);
    btnClose->setFixedSize(32, 20);
    setTitleHeight(titleHeight);
    setLogoWidth(logoWidth);
    btnLogo->setIconSize(QSize(logoWidth, titleHeight));
    // 连接
    connect(btnMin, &QPushButton::clicked, this, &JTitleBar::btnMin_clicked);
    connect(btnMax, &QPushButton::clicked, this, &JTitleBar::btnMax_clicked);
    connect(btnClose, &QPushButton::clicked, this, &JTitleBar::btnClose_clicked);
}

void JTitleBar::setTitleHeight(int height)
{
    titleHeight = height;
    setMaximumHeight(titleHeight);
    btnLogo->setMinimumHeight(titleHeight);
}

void JTitleBar::setLogoWidth(int width)
{
    btnLogo->setFixedWidth(width);
}

void JTitleBar::addJButton(JButton *button)
{
    btnList.append(button);
    hLayButtons->addWidget(button);
}

void JTitleBar::setJButtonSpacing(int size)
{
    hLayButtons->setSpacing(size);
}

void JTitleBar::setLogo(const QIcon &icon)
{
    btnLogo->setIcon(icon);
}

void JTitleBar::setLogoStyle(const QString &style)
{
    btnLogo->setStyleSheet(style);
}

void JTitleBar::setWindowMinStyle(const QString &style)
{
    btnMin->setStyleSheet(style);
}

void JTitleBar::setWindowMaxStyle(const QString &style)
{
    btnMax->setStyleSheet(style);
}

void JTitleBar::setWindowCloseStyle(const QString &style)
{
    btnClose->setStyleSheet(style);
}

void JTitleBar::btnMin_clicked()
{
    emit showMin();
}

void JTitleBar::btnMax_clicked()
{
    emit showMax();
}

void JTitleBar::btnClose_clicked()
{
    emit close();
}

void JTitleBar::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        leftButtonState = true;
        startPos = event->pos();
        emit mousePress(*event);
    }
}

void JTitleBar::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        leftButtonState = false;
        emit mouseRelease();
    }
}

void JTitleBar::mouseMoveEvent(QMouseEvent *event)
{
    if(leftButtonState) {
        emit mouseMove(event);
    }
}

void JTitleBar::mouseDoubleClickEvent(QMouseEvent *event)
{
//    qDebug() << "double click:" << event->pos();
    emit doubleClieck();
    Q_UNUSED(event)
}





/*****************主框架*****************/
JmainFrame::JmainFrame(QWidget *parent)
    : QWidget(parent)
    , widgetMain(new QWidget)
    , widgetTitle(new JTitleBar)
    , vLayMain(new QVBoxLayout)
    , WinStatus(NORMAL)
    , dragMax(false)
{
    // set objectName
    widgetTitle->setObjectName("widgetTitle");
    widgetMain->setObjectName("widgetMain");
    // layout
    vLayMain->addWidget(widgetTitle);
    vLayMain->addWidget(widgetMain);
    setLayout(vLayMain);
	/*
    JButton *btnProject = new JButton(QIcon(":/image/icon/project_empty.png"), QString::fromLocal8Bit("项目"));
    btnProject->setObjectName("btnProject");
    widgetTitle->addJButton(btnProject);
    JButton *btnAnalysis = new JButton(QIcon(":/image/icon/analysis_empty.png"), QString::fromLocal8Bit("分析"));
    btnAnalysis->setObjectName("btnAnalysis");
    widgetTitle->addJButton(btnAnalysis);
    JButton *btnSetting = new JButton(QIcon(":/image/icon/setting_empty.png"), QString::fromLocal8Bit("设置"));
    btnSetting->setObjectName("btnSetting");
    widgetTitle->addJButton(btnSetting);
    JButton *btnAbout = new JButton(QIcon(":/image/icon/about_empty.png"), QString::fromLocal8Bit("关于"));
    btnAbout->setObjectName("btnAbout");
    widgetTitle->addJButton(btnAbout);
	*/	
    // button
    JButton *btnProject = new JButton; //(QIcon(":/image/icon/project_empty.png"), QString::fromLocal8Bit("项目"));
    btnProject->setObjectName("btnProject");
    btnProject->setIcon(QIcon(":/image/icon/project_empty.png"));
    widgetTitle->addJButton(btnProject);
    JButton *btnAnalysis = new JButton; //(QIcon(":/image/icon/analysis_empty.png"), QString::fromLocal8Bit("分析"));
    btnAnalysis->setObjectName("btnAnalysis");
    btnAnalysis->setIcon(QIcon(":/image/icon/analysis_empty.png"));
    widgetTitle->addJButton(btnAnalysis);
    JButton *btnSetting = new JButton; //(QIcon(":/image/icon/setting_empty.png"), QString::fromLocal8Bit("设置"));
    btnSetting->setObjectName("btnSetting");
    btnSetting->setIcon(QIcon(":/image/icon/setting_empty.png"));
    widgetTitle->addJButton(btnSetting);
    JButton *btnAbout = new JButton; //(QIcon(":/image/icon/about_empty.png"), QString::fromLocal8Bit("关于"));
    btnAbout->setObjectName("btnAbout");
    btnAbout->setIcon(QIcon(":/image/icon/about_empty.png"));
    widgetTitle->addJButton(btnAbout);
    setGeometry(300, 300, 950, 650);
    vLayMain->setContentsMargins(2, 0, 2, 2);
    vLayMain->setSpacing(0);
    setWindowFlag(Qt::FramelessWindowHint);
    connect(widgetTitle, &JTitleBar::showMin, this, &JmainFrame::showMin);
    connect(widgetTitle, &JTitleBar::showMax, this, &JmainFrame::showMax);
    connect(widgetTitle, &JTitleBar::close, this, &JmainFrame::colseWin);
    connect(widgetTitle, &JTitleBar::doubleClieck, this, &JmainFrame::titleDoubleCilck);
    connect(widgetTitle, &JTitleBar::mousePress, this, &JmainFrame::mousePress);
    connect(widgetTitle, &JTitleBar::mouseRelease, this, &JmainFrame::mouseRelease);
    connect(widgetTitle, &JTitleBar::mouseMove, this, &JmainFrame::mouseMove);
}

JmainFrame::~JmainFrame()
{
    widgetMain->deleteLater();
    widgetTitle->deleteLater();
    vLayMain->deleteLater();
}

void JmainFrame::showMin()
{
    showMinimized();
}

void JmainFrame::showMax()
{
    titleDoubleCilck();
}

void JmainFrame::colseWin()
{
    close();
}

void JmainFrame::titleDoubleCilck()
{
    if(NORMAL == WinStatus) {
        setWindowState(Qt::WindowMaximized);
        WinStatus = MAXIMIZED;
        return;
    }
    if(MAXIMIZED == WinStatus){
        setWindowState(Qt::WindowNoState);
        WinStatus = NORMAL;
        return;
    }
}

void JmainFrame::mousePress(const QMouseEvent &sPos)
{
    startPos = sPos.pos();
}

void JmainFrame::mouseRelease()
{
    dragMax = false;
}

void JmainFrame::mouseMove(const QMouseEvent *mPos)
{
    if(dragMax)
        return;
    if(MAXIMIZED == WinStatus){
        setWindowState(Qt::WindowNoState);
        WinStatus = NORMAL;
//        move(mPos->pos().x(), mPos->globalY());
        return;
    }
    else {
        if(mPos->globalY() <= 2) {
            setWindowState(Qt::WindowMaximized);
            WinStatus = MAXIMIZED;
            dragMax = true;
            return;
        }
        else {
            QPoint p = mPos->pos() - startPos;
            move(x() + p.x(), y() + p.y());
        }
    }
}

bool JmainFrame::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType)
    int m_nBorder = 3;
    MSG *param = static_cast<MSG *>(message);
    switch (param->message)
    {
    case WM_NCHITTEST:
    {
        int nX = GET_X_LPARAM(param->lParam) - this->geometry().x();
        int nY = GET_Y_LPARAM(param->lParam) - this->geometry().y();

        // 如果鼠标位于子控件上，则不进行处理
        if (childAt(nX, nY) != nullptr) {
            setCursor(Qt::ArrowCursor);
            return QWidget::nativeEvent(eventType, message, result);
        }

        *result = HTCAPTION;

        // 鼠标区域位于窗体边框，进行缩放
        if ((nX > 0) && (nX < m_nBorder)) {
            setCursor(Qt::SizeHorCursor);
            *result = HTLEFT;
        }

        if ((nX > this->width() - m_nBorder) && (nX < this->width())) {
            setCursor(Qt::SizeHorCursor);
            *result = HTRIGHT;
        }

        if ((nY > this->height() - m_nBorder) && (nY < this->height())) {
            setCursor(Qt::SizeVerCursor);
            *result = HTBOTTOM;
        }

        if ((nX > 0) && (nX < m_nBorder) && (nY > 0) && (nY < m_nBorder)) {
            *result = HTTOPLEFT;
        }

        if ((nX > this->width() - m_nBorder) && (nX < this->width())
                && (nY > 0) && (nY < m_nBorder)) {
            *result = HTTOPRIGHT;
        }

        if ((nX > 0) && (nX < m_nBorder)
                && (nY > this->height() - m_nBorder) && (nY < this->height())) {
            *result = HTBOTTOMLEFT;
        }

        if ((nX > this->width() - m_nBorder) && (nX < this->width())
                && (nY > this->height() - m_nBorder) && (nY < this->height())) {
            *result = HTBOTTOMRIGHT;
        }

        return true;
    }
    }
//    setCursor(Qt::ArrowCursor);
    return QWidget::nativeEvent(eventType, message, result);
}

